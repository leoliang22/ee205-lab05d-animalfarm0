///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////


#include<stdio.h>
//include"catDatabase.c"
extern size_t numCats;

void deleteAllCats(void){
   numCats=0;
}
