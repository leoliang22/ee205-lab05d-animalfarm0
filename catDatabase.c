///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////


//#ifndef __catDatabase_H
//#define __catDatabase_h
#include <stdio.h>
#include "catDatabase.h"
#include <stdbool.h>


char catNames[max_cats][max_length];
enum genders {UNKNOWN_GENDER, MALE, FEMALE, OTHER};
enum breeds {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
int catGender[max_cats];
int catBreed[max_cats];
bool is_fixed[max_cats];  
float catWeight[max_cats];  
size_t numCats  = 0; 

