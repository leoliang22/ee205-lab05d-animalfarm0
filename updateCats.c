///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////


#include<stdio.h>
#include<string.h>
#include<stdbool.h>
//#ifndef CATDATABASE_C
//#define CATDATABASE_C
//#endif
//#include"catDatabase.c"
extern char catNames[];
extern float catWeight[];
extern int max_length;
extern bool is_fixed[];

int updateCatName(index, newName){
   char *str = catNames;
   char* p;
   p=strstr(str, p);
   if(strlen(newName) == 0){
      return 1;
   }
   else if (p){
   return 1;
   }
   else{
   memset(catNames[index], 0, max_length);
   strcpy(catNames[index], newName);
   return 0;

   }


int fixCat(index){
   is_fixed[index]=1;
   return 0;
}

int updateCatWeight(index, newWeight){
   if (newWeight < 0){
      return 1;
   }
   else{
      catWeight[index]= newWeight;
      return 0;
   }
}
}

