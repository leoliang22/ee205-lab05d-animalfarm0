###############################################################################
#         University of Hawaii, College of Engineering
# @brief  Lab 05d- Animal Farm 0 - EE 205 - Spr 2022
#
# @file    Makefile
# @version 1.0
#
# @author Leo Liang <leoliang@hawaii.edu>
# @date   21_2_2022
#
# @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

TARGET = animalFarm


all:  $(TARGET)


CC     = gcc
CFLAGS = -Wall -Wextra $(DEBUG_FLAGS)


debug: DEBUG_FLAGS = -g -DDEBUG
debug: clean $(TARGET)

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c catDatabase.h

animalFarm.o: addCats.c catDatabase.c deleteCats.c updateCats.c reportCats.c main.c
	$(CC) $(CFLAGS) -c addCats.c catDatabase.c deleteCats.c updateCats.c reportCats.c main.c


animalFarm: addCats.o catDatabase.o deleteCats.o updateCats.o reportCats.o main.o
	$(CC) $(CFLAGS) -o $(TARGET) addCats.o catDatabase.o deleteCats.o updateCats.o reportCats.o main.o


test: $(TARGET)
	./$(TARGET)


clean:
	rm -f $(TARGET) *.o
