///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <string.h>
//#include "addCat.h"
#include <stdbool.h>
//#ifndef CATDATABASE_C
//#define CATDATABASE_C
//#endif
//#include "catDatabase.c"

extern size_t numCats;
extern int max_cats;
extern int max_length;
extern int catGender[];
extern int catBreed[];
extern float catWeight[];
extern bool is_fixed[];
extern char catNames[];
extern enum genders;
extern enum breeds;

int addCat(char name, int genders, int breeds, bool isFixed ,float weight){
   
   if (numCats >= max_cats){
      return 1;
   }

   //Add name
   if(strlen(name)==0){
      return 1;
   }
   else if (strlen(name) > max_length){
      return 1;
   }
   else if (strstr(catNames, name)== NULL){
      return 1;
   }
   else{
   strcat(catNames[numCats], name);
   }
   //add gender
   //strcat(catGender[numCats], enum genders x);
   catGender[numCats] = genders;
   //add breed
   //strcat(catBreed[numCats], enum breeds y);
   catBreed[numCats] =  breeds;
   //add isFixed
   is_fixed[numCats] = isFixed;
   //add weight
   if (weight < 0){
      return 1;
   }
   else{
   catWeight[numCats] = weight;
   }

   return numCats++;
}
   
