///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - AnimalFarm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Leo Liang <leoliang@hawaii.edu>
/// @date 15_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include<stdio.h>
#include<string.h>
#ifndef CATDATABASE_C
#define CATDATABASE_C
#endif
#include<stdbool.h>
//#include "catDatabase.c"
extern size_t numCats;
extern int max_cats;
extern int max_length;
extern int catGender[];
extern int catBreed[];
extern float catWeight[];
extern bool is_fixed[];
extern char catNames[];



int printCat(index){
   if(index<0){
      printf("animalFarm0: Bad cat [%d]" ,index);
   }
   else if(index> numCats){
      printf("animalFarm0: Bad cat [%d]" ,index);
   }
   else{
      printf("cat index = [%ld]", index);
      printf("name =[%s]", catNames[index]);
      printf("gender= [%d]", catGender[index]);
      printf("breed =[%d]", catBreed[index]);
      printf("isFixed = [%d]",is_fixed[index]);
      printf("weight = [%f]", catWeight[index]);
}
return 0;
}

void printAllCats(void){
   for (int i=0; i <= numCats; i++){
      printf("cat index = [%ld]", i);
      printf("name =[%s]", catNames[i]);
      printf("gender= [%d]", catGender[i]);
      printf("breed =[%d]", catBreed[i]);
      printf("isFixed = [%d]",is_fixed[i]);
      printf("weight = [%f]", catWeight[i]);
}
}
int findCat(char name){
   char* num;
   char* namesinarray= catNames;
   num = strstr(namesinarray, name);
   if (num){
      int position= num - name;
      return position;
   }
   else{
      return 1;
   }
}
